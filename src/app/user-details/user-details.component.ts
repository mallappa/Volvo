
import { FilterPipe } from './../filter.pipe';
import { UsersService } from './../users.service';
import { Component, OnInit } from '@angular/core';
import { Users } from '../add-user/user';
import { Router } from '@angular/router';



@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {


  constructor(private userSer:UsersService, private router: Router) { }
  users:Users[]; 
  searchText 
  ngOnInit() {
    this.getUsers();
    localStorage.clear();
  }

  getUsers(){
    this.userSer.getUsers().subscribe(data=>this.users=data) 
 }
 editUser(user){
    console.log(`${JSON.stringify(user)}`);
    localStorage.setItem('id',user.id);
    this.router.navigate(['add-user']);
    
 }

 deleteUser(user){
    this.userSer.deleteUser(user.id).subscribe(data=>data)
    this.getUsers()
 }

//  search(searchkey){
//    console.log("searchkey"+searchkey);
//    this.users.filter(this.users.filter((searchkey)=>{
//        return this.users.name==searchkey;
//    }));   

//  }
 clear(){
   this.searchText=''
 }

}

export class Users{
    name?: string;
    surname?:string;  
    birthDate?: number;
    phone?:string;
    city?:string;
    street?:string;  
    id?: number;      
    number?: number;      
}
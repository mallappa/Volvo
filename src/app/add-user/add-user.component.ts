import { UsersService } from './../users.service';
import { Users } from './user';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(private userSer: UsersService, private router: Router, private fb: FormBuilder) { 
    this.createForm();
     this.editableId=localStorage.getItem('id');
    // 
  }
  userformlabel: string = 'Add User';  
  userformbtn: string = 'Save';  

  addUser: FormGroup;  
  btnvisibility: boolean = true; 
  editableId;

  ngOnInit() {
    this.createForm();
    this.getEmployeeToEdit();
  }
  
  createForm(){
    this.addUser = this.fb.group({      
      id: [],  
      name: ['', Validators.required],  
      surname: ['', Validators.required],  
      birthDate: ['', [Validators.required]],  
      phone: ['', [Validators.required, Validators.maxLength(10)]],  
      city: ['', [Validators.required, Validators.maxLength(20)]],  
      street: ['', [Validators.required, Validators.maxLength(25)]],  
      number: ['', [Validators.required, Validators.maxLength(25)]],  
      // employee_age: ['', [Validators.required, Validators.maxLength(3)]]  
      
    })
  }
  
  getEmployeeToEdit(){
    if(this.editableId!=undefined&&this.editableId!=null)
    this.btnvisibility=false
  this.userSer.getUserById(this.editableId).subscribe(data=> {
    this.addUser.controls['id'].setValue(data.id);
    this.addUser.controls['name'].setValue(data.name);
    this.addUser.controls['surname'].setValue(data.surname);
    this.addUser.controls['birthDate'].setValue(data.birthDate);
    this.addUser.controls['phone'].setValue(data.phone);
    this.addUser.controls['city'].setValue(data.city);
    this.addUser.controls['street'].setValue(data.street);
    this.addUser.controls['number'].setValue(data.number);
  })
  
  }
  onSubmit(){
    console.log(`${JSON.stringify(this.addUser.value)}`);
        this.userSer.createUser(JSON.stringify(this.addUser.value)).subscribe(data=>data)
        this.router.navigate(['users']);
  }

  onUpdate(){
    console.log(`onupdate  ${JSON.stringify(this.addUser.value)}`);
    this.userSer.updateUser(this.addUser.value).subscribe(data=>data)
    localStorage.clear();
    this.router.navigate(['users']);
  }
}





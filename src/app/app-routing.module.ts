
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Routes } from '@angular/router';

import{
// components
AddUserComponent,
UserDetailsComponent
} from './index'

export const routes:Routes=[
  {path:'',redirectTo:'users', pathMatch:'full'},
  {path:'users',component:UserDetailsComponent},
  {path:'add-user',component:AddUserComponent},
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  declarations: [],
  exports:[RouterModule]

})
export class AppRoutingModule { }

import { UsersService } from './users.service';
import { AddUserComponent } from './add-user/add-user.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { HttpClientModule } from '@angular/common/http';  
import { AppRoutingModule } from './app-routing.module';  
import { ReactiveFormsModule,FormsModule } from "@angular/forms";  

import { AppComponent } from './app.component';  
import { Ng2SearchPipeModule } from 'ng2-search-filter';



import{
  // components
  // AddUserComponent,
  UserDetailsComponent,
  
  // services
  // UsersService
  } from './index';
import { FilterPipe } from './filter.pipe'

@NgModule({
  declarations: [
    AppComponent,
    AddUserComponent,
    UserDetailsComponent,
    FilterPipe
    
],
  imports: [
    BrowserModule,
    HttpClientModule,  
    ReactiveFormsModule,
    AppRoutingModule,
    FormsModule,
    Ng2SearchPipeModule

    
  ],
  providers: [UsersService],
  
  bootstrap: [AppComponent]
})
export class AppModule { }


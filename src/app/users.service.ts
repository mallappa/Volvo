
import { Injectable } from '@angular/core';  
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Http, Response, Headers, } from '@angular/http';

import { map } from 'rxjs/operators';
import { Users } from './add-user/user';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class UsersService {

constructor(private http: HttpClient) { }
baseUrl: string = 'http://localhost:3000/'; 
 header = { headers: new HttpHeaders().set('Content-Type', 'application/json' ) }; 
getUsers(){
  return this.http.get<Users[]>(`${this.baseUrl}names`,this.header);
}
deleteUser(id: number) {  
  return this.http.delete<Users[]>(`${this.baseUrl}names`+'/' + id);  
}  
createUser(user) {  
  return this.http.post(`${this.baseUrl}names`, user,this.header);  
}  
getUserById(id: number) {  
  return this.http.get<Users>(`${this.baseUrl}names` + '/' + id);  
}  
updateUser(user: Users) {  
  return this.http.put<Users[]>(`${this.baseUrl}names` + '/' + user.id, user);  
} 
}

import { Pipe, PipeTransform } from '@angular/core';
import { Users } from './add-user/user';


@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  
  transform(users: Users[], searchText?: string): any {
    if (!users || !searchText){
      return users;
    }
    return users.filter(user=>
      user.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
  }

}
